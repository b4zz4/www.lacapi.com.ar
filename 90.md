[Posters](index.html%3Fp=90.html#post)
--------------------------------------

![image](http://www.lacapi.com.ar/wp-content/uploads/2007/11/poster.gif)
If you like the images of the page, **you can get a copy** of 32cm x
55cm (12.6” x 21.6”) for just $20.\
 This way, you can collaborate with the developing of this page and with
the Widget of La Capi.

size

A4 – 21cm x 30cm $20,00

A3 – 30cm x 42cm $30,00

A2 – 42cm x 60cm $40,00

15 Sticker $15,00\
